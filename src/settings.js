module.exports = {
  title: '尚书',

  /**
   * 是否显示右侧设置工具
   */
  showSettings: true,

  /**
   * 是否启用导航视图
   */
  tagsView: true,

  /**
   * 导航是否是绝对定位
   */
  fixedHeader: false,

  /**
   * 是否显示logo
   */
  sidebarLogo: false,

  /**
   * 是否显示错误组件 可选值：['production', 'development']，默认值：production
   */
  errorLog: 'development'
}
