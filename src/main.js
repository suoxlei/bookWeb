import Vue from 'vue'

import Element from 'element-ui'

import Cookies from 'js-cookie'
// 一种CSS reset的替代方案，提供了跨浏览器的高度一致性
import 'normalize.css/normalize.css'
import './styles/element-variables.scss'
// 全局css样式文件
import '@/styles/index.scss'
import '@/styles/admin.scss'

import App from './App'
import store from './store'
import router from './router'
import permission from './directive/permission'
import { getDictByType } from '@/api/system/dict/data'
import { getConfigKey } from '@/api/system/config'
import { addDateRange, parseTime, resetForm, selectDictLabel } from '@/utils/costum'
// icon
import './icons'
// 权限控制
import './permission'
// 错误日志
import './utils/error-log'
// 全局filters
import * as filters from './filters'
// 分页组件
import Pagination from '@/components/Pagination'

// 全局方法挂载
Vue.prototype.getDictByType = getDictByType
Vue.prototype.getConfigKey = getConfigKey
Vue.prototype.parseTime = parseTime
Vue.prototype.resetForm = resetForm
Vue.prototype.addDateRange = addDateRange
Vue.prototype.selectDictLabel = selectDictLabel

// 全局组件挂载
Vue.component('Pagination', Pagination)

// 成功消息提示
Vue.prototype.msgSuccess = function(msg) {
  this.$message({ showClose: true, message: msg, type: 'success' })
}

// 错误消息提示
Vue.prototype.msgError = function(msg) {
  this.$message({ showClose: true, message: msg, type: 'error' })
}

// 常规消息提示
Vue.prototype.msgInfo = function(msg) {
  this.$message.info(msg)
}

Vue.use(permission).use(Element, {
  // 设置element-ui默认字体大小
  size: Cookies.get('size') || 'medium'
})

// 注册全局应用程序过滤器
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

// 阻止启动生产消息，常用作指令。
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
