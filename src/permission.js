import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
// 通过cookie获取token
import { getToken } from '@/utils/auth'
import getPageTitle from '@/utils/get-page-title'

NProgress.configure({ showSpinner: true })

// 白名单
const whiteList = ['/login', '/auth-redirect']

router.beforeEach(async(to, from, next) => {
  NProgress.start()
  document.title = getPageTitle(to.meta.title)

  // 确定用户是否已经登陆
  if (getToken()) {
    if (to.path === '/login') {
      // 如果已经登陆跳转到主页
      next({ path: '/' })
      NProgress.done()
    } else {
      // 确定用户是否已经通过getInfo接口获得权限
      if (store.getters.user_id > 0) {
        next()
      } else {
        try {
          await store.dispatch('user/getProfile')
          const accessRoutes = await store.dispatch('permission/generateRoutes')
          // 动态添加可访问路由
          router.addRoutes(accessRoutes)
          // 设置replace: true, 这样导航就不会留下历史记录
          next({ ...to, replace: true })
        } catch (error) {
          // // 移除token重新登陆
          await store.dispatch('user/resetToken')
          Message.error(error || 'Has Error')
          next(`/login?redirect=${to.path}`)
          NProgress.done()
        }
      }
    }
  } else {
    // 没有token
    if (whiteList.indexOf(to.path) !== -1) {
      next()
    } else {
      next(`/login?redirect=${to.path}`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  NProgress.done()
})
