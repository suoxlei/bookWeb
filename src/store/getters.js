const getters = {
  sidebar: state => state.app.sidebar,
  size: state => state.app.size,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  errorLogs: state => state.errorLog.logs,
  token: state => state.user.token,
  user_id: state => state.user.user_id,
  mobile: state => state.user.mobile,
  real_name: state => state.user.real_name,
  is_super_admin: state => state.user.is_super_admin,
  permissions: state => state.permission.permissions,
  permission_routes: state => state.permission.routes,
}
export default getters
