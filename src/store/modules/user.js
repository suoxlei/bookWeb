import {getProfile, login, logout, refreshtoken} from '@/api/user'
import {getToken, removeToken, setSecret, setToken} from '@/utils/auth'
import store from "@/store";
import Vuex from "vuex";
import getters from "@/store/getters";

const state = {
  token: getToken(),
  user_id: 0,
  mobile: '',
  real_name: '',
  is_super_admin: 0,
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_USER_ID: (state, user_id) => {
    state.user_id = user_id
  },
  SET_MOBILE: (state, mobile) => {
    state.mobile = mobile
  },
  SET_REAL_NAME: (state, real_name) => {
    state.real_name = real_name
  },
  SET_IS_SUPER_ADMIN: (state, is_super_admin) => {
    state.is_super_admin = is_super_admin
  },
  SET_LOAD_MENU: (state, load_menu) => {
    state.load_menu = load_menu
  },
}

const actions = {
  // 用户登陆
  login({commit}, userInfo) {
    return new Promise((resolve, reject) => {
      login(userInfo).then(response => {
        const {token} = response.data
        commit('SET_TOKEN', token)
        setToken(token)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 获取个人资料
  getProfile({ commit, state }) {
    return new Promise(resolve => {
      getProfile().then(response => {
        if (!response || !response.data) {
          commit('SET_TOKEN', '')
          removeToken()
          resolve()
        }

        const { id,user_code,mobile, nick_name,real_name, user_title, is_super_admin} = response.data
        if (!id || id <= 0) {
          reject('获取个人资料: id must be a int!')
        }
        commit('SET_USER_ID', id)
        commit('SET_MOBILE', mobile)
        commit('SET_REAL_NAME', real_name)
        commit('SET_IS_SUPER_ADMIN', is_super_admin)

        resolve(response)
      }).catch(error => {
        reject(error)
      })
      resolve()
    })
  },


  // 退出系统
  logout({commit, state}) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      commit('SET_USER_ID', '')
      commit('SET_MOBILE', '')
      commit('SET_REAL_NAME', '')
      commit('SET_IS_SUPER_ADMIN', 0)
      commit('SET_LOAD_MENU', '')
      removeToken()
      resolve()
    })
  },
}


export default {
  namespaced: true,
  state,
  mutations,
  actions
}
