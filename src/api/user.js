import request from '@/utils/request'

// 获取钉钉验证码
export function getDingTalkCode(data) {
  return request({
    url: '/user/dingtalk/send/code',
    method: 'post',
    data
  })
}

export function login(data) {
  return request({
    url: '/user/login',
    method: 'post',
    data
  })
}

export function getProfile(data) {
  return request({
    url: '/user/get/profile',
    method: 'get',
  })
}
