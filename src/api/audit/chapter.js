//查询章节信息
import request from "@/utils/request";

export function getChapterTask() {
  return request({
    url: '/audit/chapter/task',
    method: 'get',
  })
}
