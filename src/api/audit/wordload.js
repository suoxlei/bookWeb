// 查询平台列表
import request from "@/utils/request";

export function getWordload(query) {
  return request({
    url: '/audit/workload',
    method: 'get',
    params: query
  })
}
