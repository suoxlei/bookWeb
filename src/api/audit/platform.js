
import request from "@/utils/request";

// 查询平台列表
export function getPlatform(query) {
  return request({
    url: '/audit/platform/list',
    method: 'get',
    params: query
  })
}

// 查询平台列表
export function getPlatformInfo(id) {
  return request({
    url: '/audit/platform/info?id=' + id,
    method: 'get',
  })
}

// 修改平台
export function updatePlatform(data) {
  return request({
    url: '/audit/platform/update',
    method: 'post',
    data: data
  })
}


