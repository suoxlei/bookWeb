// 查询book列表
import request from "@/utils/request";

//查询已审核书籍列表
export function getAuditedList(query) {
  return request({
    url: '/audit/book/audited/list',
    method: 'get',
    params: query
  })
}

//查询已审核书籍列表
export function getBookTask() {
  return request({
    url: '/audit/book/audit/task',
    method: 'get',
  })
}

// 待审核书籍通过
export function approvalAuditBook(data) {
  return request({
    url: '/audit/book/audit/approval',
    method: 'post',
    data: data
  })
}

// 待审核书籍放弃
export function abandonAuditBook(data) {
  return request({
    url: '/audit/book/audit/abandon',
    method: 'post',
    data: data
  })
}


// 更新已审核书籍
export function updateAuditedBook(data) {
  return request({
    url: '/audit/book/audited/update',
    method: 'post',
    data: data
  })
}

