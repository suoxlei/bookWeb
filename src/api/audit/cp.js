// 查询CP详细
import request from "@/utils/request";

// 查询cp列表
export function getCpList(query) {
  return request({
    url: '/audit/cp/list',
    method: 'get',
    params: query
  })
}

// 查询cp列表
export function getCpInfo(id) {
  return request({
    url: '/audit/cp/info?id=' + id,
    method: 'get',
  })
}

// 修改CP
export function updateCp(data) {
  return request({
    url: '/audit/cp/update',
    method: 'post',
    data: data
  })
}


// 用户状态修改
export function changeCpStatus(cpId, status) {
  return request({
    url: '/audit/cp/change/status',
    method: 'post',
    data: { id:cpId,status:status }
  })
}





