// 敏感词
import request from "@/utils/request";

export function getCategory() {
  return request({
    url: '/audit/sensitive/category',
    method: 'get',
  })
}


// 添加敏感词
export function addSensitive(data){
  return request({
    url: '/audit/sensitive/add',
    method: 'post',
    data: data
  })
}

// 修改平台
export function removeSensitive(data) {
  return request({
    url: '/audit/sensitive/remove',
    method: 'post',
    data: data
  })
}
