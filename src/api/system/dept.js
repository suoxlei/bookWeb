import request from '@/utils/request'

export function listDept(query) {
  return request({
    url: '/api/v1/dept',
    method: 'get',
    params: query
  })
}

// 查询部门详细
export function getDept(deptId) {
  return request({
    url: '/api/v1/dept/info/' + deptId,
    method: 'get'
  })
}

// 查询部门下拉树结构
export function deptTree() {
  return request({
    url: '/api/v1/dept/tree',
    method: 'get'
  })
}

// 根据角色ID查询部门树结构
export function roleDeptTree(roleId) {
  return request({
    url: '/api/v1/dept/role/select/' + roleId,
    method: 'get'
  })
}

// 新增部门
export function addDept(data) {
  return request({
    url: '/api/v1/dept',
    method: 'post',
    data: data
  })
}

// 修改部门
export function updateDept(data) {
  return request({
    url: '/api/v1/dept/' + data['dept_id'],
    method: 'put',
    data: data
  })
}

// 删除部门
export function deleteDept(deptId) {
  return request({
    url: '/api/v1/dept/' + deptId,
    method: 'delete'
  })
}
