import request from '@/utils/request'

// 根据手机号获取用户信息
export function getUserByMobile(mobile) {
  return request({
    url: '/api/v1/user/mobile/' + mobile,
    method: 'get'
  })
}

// 查询用户列表
export function getUserList(query) {
  return request({
    url: '/api/v1/user',
    method: 'get',
    params: query
  })
}

// 查询用户详细
export function getUser(userId) {
  return request({
    url: '/api/v1/user/info/' + userId,
    method: 'get'
  })
}

export function getUserProfile() {
  return request({
    url: '/api/v1/user/profile',
    method: 'get'
  })
}

// 新增用户
export function addUser(data) {
  return request({
    url: '/api/v1/user',
    method: 'post',
    data: data
  })
}

// 修改用户
export function updateUser(data) {
  return request({
    url: '/api/v1/user/update/' + data.user_id,
    method: 'put',
    data: data
  })
}

// 删除用户
export function delUser(userId) {
  return request({
    url: '/api/v1/user/delete/' + userId,
    method: 'delete'
  })
}

// 导出用户
export function exportUser(query) {
  return request({
    url: '/api/v1/sysUser/export',
    method: 'get',
    params: query
  })
}

// 用户密码重置
export function resetUserPwd(userId, password) {
  return request({
    url: '/api/v1/user/update/' + userId,
    method: 'put',
    data: { password }
  })
}

// 用户状态修改
export function changeUserStatus(userId, status) {
  return request({
    url: '/api/v1/user/update/' + userId,
    method: 'put',
    data: { status }
  })
}

// 修改用户个人信息
export function updateUserProfile(data) {
  return request({
    url: '/api/v1/sysUser/profile',
    method: 'put',
    data: data
  })
}

// 用户密码重置
export function updateUserPwd(oldPassword, newPassword) {
  return request({
    url: '/api/v1/user/password',
    method: 'put',
    data: { old_password: oldPassword, new_password: newPassword }
  })
}

// 用户头像上传
export function uploadAvatar(data) {
  return request({
    url: '/api/v1/user/avatar',
    method: 'post',
    data: data
  })
}
