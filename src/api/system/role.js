import request from '@/utils/request'

// 查询角色列表
export function listRole(query) {
  return request({
    url: '/api/v1/role',
    method: 'get',
    params: query
  })
}

// 查询角色详细
export function getRole(roleId) {
  return request({
    url: '/api/v1/role/info/' + roleId,
    method: 'get'
  })
}

// 新增角色
export function addRole(data) {
  return request({
    url: '/api/v1/role',
    method: 'post',
    data: data
  })
}

// 修改角色
export function updateRole(data) {
  return request({
    url: '/api/v1/role/' + data['role_id'],
    method: 'put',
    data: data
  })
}

// 角色数据权限
export function updateDataScope(data) {
  return request({
    url: '/api/v1/role/' + data.role_id + '/datascope',
    method: 'put',
    data: data
  })
}

// 角色状态修改
export function changeRoleStatus(roleId, status) {
  const data = {
    status
  }
  return request({
    url: '/api/v1/role/' + roleId + '/status',
    method: 'put',
    data: data
  })
}

// 删除角色
export function deleteRole(roleId) {
  return request({
    url: '/api/v1/role/' + roleId,
    method: 'delete'
  })
}

export function getRoutes() {
  return request({
    url: '/user/menu/list',
    method: 'get'
  })
}
