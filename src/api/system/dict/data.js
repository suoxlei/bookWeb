import request from '@/utils/request'

// 查询字典数据列表
export function listDictData(query) {
  return request({
    url: '/api/v1/dict/datalist?dict_type=' + query.dict_type,
    method: 'get',
    params: query
  })
}

// 查询字典数据详细
export function getDictData(dictCode) {
  return request({
    url: '/api/v1/dict/data/' + dictCode,
    method: 'get'
  })
}

// 根据字典类型查询字典数据信息
export function getDictByType(dictType) {
  return request({
    url: '/api/v1/dict/bytype/' + dictType,
    method: 'get'
  })
}

// 新增字典数据
export function addDictData(data) {
  return request({
    url: '/api/v1/dict/data',
    method: 'post',
    data: data
  })
}

// 修改字典数据
export function updateDictData(data) {
  return request({
    url: '/api/v1/dict/data/' + data['id'],
    method: 'put',
    data: data
  })
}

// 删除字典数据
export function deleteDictData(dictCode) {
  return request({
    url: '/api/v1/dict/data/' + dictCode,
    method: 'delete'
  })
}
