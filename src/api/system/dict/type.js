import request from '@/utils/request'

// 查询字典类型列表
export function listDictType(query) {
  return request({
    url: '/api/v1/dict/typelist',
    method: 'get',
    params: query
  })
}

// 查询字典类型详细
export function getDictType(dictId) {
  return request({
    url: '/api/v1/dict/type/' + dictId,
    method: 'get'
  })
}

// 新增字典类型
export function addDictType(data) {
  return request({
    url: '/api/v1/dict/type',
    method: 'post',
    data: data
  })
}

// 修改字典类型
export function updateDictType(data) {
  return request({
    url: '/api/v1/dict/type/' + data['id'],
    method: 'put',
    data: data
  })
}

// 删除字典类型
export function deleteDictType(dictId) {
  return request({
    url: '/api/v1/dict/type/' + dictId,
    method: 'delete'
  })
}
