import request from '@/utils/request'

// 查询操作日志列表
export function operlogList(query) {
  return request({
    url: '/api/v1/operlog/list',
    method: 'get',
    params: query
  })
}

// 删除操作日志
export function deleteOperlog(id) {
  return request({
    url: '/api/v1/operlog/delete' + id,
    method: 'delete'
  })
}

// 清空操作日志
export function cleanOperlog() {
  return request({
    url: '/api/v1/operlog/clean',
    method: 'delete'
  })
}

// 导出操作日志
export function exportOperlog(query) {
  return request({
    url: '/api/v1/operlog/export',
    method: 'get',
    params: query
  })
}
