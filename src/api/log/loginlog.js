import request from '@/utils/request'

// 查询登录日志列表
export function loginlogList(query) {
  return request({
    url: '/api/v1/loginlog/list',
    method: 'get',
    params: query
  })
}

// 删除登录日志
export function deleteLoginlog(id) {
  return request({
    url: '/api/v1/loginlog/delete/' + id,
    method: 'delete'
  })
}

// 清空登录日志
export function cleanLoginlog() {
  return request({
    url: '/api/v1/loginlog/clean',
    method: 'delete'
  })
}

// 导出登录日志【服务端暂未实现】
export function exportLoginlog(query) {
  return request({
    url: '/api/v1/loginlog/export',
    method: 'get',
    params: query
  })
}
