
import request from "@/utils/request";

// 查询用户组列表
export function groupList(query) {
  return request({
    url: '/user/group/list',
    method: 'get',
    params: query
  })
}

// 查询所有菜单下拉树结构
export function menuTree(group_id) {
  return request({
    url: '/user/permission/tree?group_id=' + group_id,
    method: 'get',
  })
}

// 新增用户组
export function addGroup(data) {
  return request({
    url: '/user/group/add',
    method: 'post',
    data: data
  })
}

// 修改用户组
export function updateGroup(data) {
  return request({
    url: '/user/group/update',
    method: 'post',
    data: data
  })
}


// 查询用户组详细信息
export function getGroup(roleId) {
  return request({
    url: '/user/group/get?id=' + roleId,
    method: 'get'
  })
}

// 根据角色ID查询菜单下拉树结构
export function groupMenuTree(roleId) {
  return request({
    url: '/api/v1/menu/role/menutree/' + roleId,
    method: 'get'
  })
}

// 删除角色
export function deleteGroup(ids) {
  return request({
    url: '/user/group/delete',
    method: 'post',
    data: {ids:ids}
  })
}

