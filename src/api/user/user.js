// 查询部门详细
import request from "@/utils/request";

// 查询用户列表
export function getUserList(query) {
  return request({
    url: '/user/list',
    method: 'get',
    params: query
  })
}

// 用户状态修改
export function changeUserStatus(userId, status) {
  return request({
    url: '/user/change/status',
    method: 'post',
    data: { id:userId,status:status }
  })
}

// 新增用户
export function addUser(data) {
  return request({
    url: '/user/add',
    method: 'post',
    data: data
  })
}

//根据id获取用户详细信息
export function getUser(id) {
  return request({
    url: '/user/get/user?id=' + id,
    method: 'get',
  })
}

// 修改用户
export function updateUser(data) {
  return request({
    url: '/user/update',
    method: 'post',
    data: data
  })
}

// 删除用户
export function delUser(ids) {
  return request({
    url: '/user/delete/',
    method: 'post',
    data: {ids:ids}
  })
}

//根据可选择用户详细信息
export function getSelectUser() {
  return request({
    url: '/user/dingtalk/list',
    method: 'get',
  })
}

// 添加选中的所有用户
export function addSelectUser(data) {
  return request({
    url: '/user/selected/add',
    method: 'post',
    data: data
  })
}


