// 查询菜单列表
import request from "@/utils/request";

// 新增菜单
export function addPermission(data) {
  return request({
    url: '/user/permission/add',
    method: 'post',
    data: data
  })
}

// 修改菜单
export function updatePermission(data) {
  return request({
    url: '/user/permission/update',
    method: 'post',
    data: data
  })
}

// 查询菜单详细
export function getPermission(permissionId) {
  return request({
    url: '/user/permission/info?id=' + permissionId,
    method: 'get'
  })
}

// 修改菜单
export function deletePermission(data) {
  return request({
    url: '/user/permission/delete',
    method: 'post',
    data: data
  })
}
