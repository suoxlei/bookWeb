import axios from 'axios'
import { Message, MessageBox } from 'element-ui'
import store from '@/store'
import { getToken } from '@/utils/auth'
import { createUniqueString, getSign } from '@/utils/index'
// import CryptoJS from 'crypto-js'

// 创建一个axios实例
const service = axios.create({
  // url = base url + request url
  baseURL: process.env.VUE_APP_BASE_API,
  // baseURL: 'http://www.check.com',
  // 跨站请求发送cookie
  // withCredentials: true,
  // 超时时间
  timeout: 10000
})

// 请求拦截器
service.interceptors.request.use(
  config => {
    // console.log('发出请求之前做的事情', config)
    // 发出请求之前做的事情

    if (store.getters.token) {
      // 每次请求携带token
      // Authorization是自定义的请求头
      // 可根据实际情况修改
      config.headers['Authorization'] = getToken()

      // // 接口请求增加签名
      // const nonce = createUniqueString()
      // const timestamp = parseInt(new Date().getTime() / 1000)
      //
      // // 无需验证签名接口，请求url中无需额外增加参数
      // const allowUrl = ['/api/v1/user/login']
      // if (!allowUrl.includes(config.url)) {
      //   const data = {}
      //   if (config.params) {
      //     for (const k in config.params) {
      //       if (config.params[k] !== undefined) {
      //         data[k] = config.params[k]
      //       }
      //     }
      //   }
      //   data.nonce = nonce
      //   data.timestamp = timestamp.toString()
      //   config.url += (config.url.indexOf('?') > 0 ? '&' : '?') + 'nonce=' + nonce + '&timestamp=' + timestamp + '&sign=' + getSign(data, store.getters.secret)
      // }
    }

    return config
  },
  error => {
    // 处理请求错误
    // console.log(error)
    return Promise.reject(error)
  }
)

// 响应拦截器
service.interceptors.response.use(
  /**
   * 如果想要获取header或状态码等信息，请返回response => response
   */

  /**
   * 可以通过http状态码确定处理机制
   */
  response => {
    const code = response.data.code
    if (code === 106) {
      MessageBox.confirm(
        '登录状态已过期，您可以继续留在该页面，或者重新登录',
        '系统提示',
        {
          confirmButtonText: '重新登录',
          cancelButtonText: '取消',
          type: 'warning'
        }
      ).then(() => {
        location.reload() // 为了重新实例化vue-router对象 避免bug
      })
      return false
    } else if (code !== 200) {
      /* Notification.error({
        title: response.data.message
      })*/
      Message({
        message: response.data.message,
        type: 'error',
        duration: 5 * 1000
      })
      return Promise.reject('error')
    } else {
      return response.data
    }
  },
  error => {
    if (error.message === 'Network Error') {
      Message({
        message: '服务器连接异常，请联系系统管理员检查服务端！',
        type: 'error',
        duration: 5 * 1000
      })
      return
    }
    console.log('err' + error) // for debug

    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })

    return Promise.reject(error)
  }
)

export default service
